/**
 * Home selectors
 */

import { createSelector } from 'reselect';

// const publishedSmartFramesSelector = (state) => (state.home ? state.home.publishedSmartFrames : []);

// const getPublishedSmartFrames = () => createSelector(
//     publishedSmartFramesSelector,
//     (publishedSmartFrames) => publishedSmartFrames.map((publishedSmartFrame) => ({ ...publishedSmartFrame }))
// );
const imageSourceSelector = (state) => (state.trainer? state.trainer.imageSource : "" );
const getImageSourceSelector = () => { createSelector(
    imageSourceSelector,
    (imageSource) => { return imageSource }
)};
export {
    // getPublishedSmartFrames
    getImageSourceSelector,
    imageSourceSelector
};

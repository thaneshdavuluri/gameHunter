import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from './trainer.reducer';
import { getImageSource,setImageSource } from './trainer.actions';
// import {  getImageSourceSelector} from './trainer.selector';
import TrainerComponent from './trainer.component';

const mapDispatchToProps = (dispatch) => ({
    getImageSource: () => dispatch(getImageSource()),
    setImageSource: (source) => dispatch(setImageSource(source)),
});

// const mapStateToProps = createStructuredSelector({
//     imageSource: getImageSourceSelector()
// });
const mapStateToProps = (state) => {
    return {imageSource:  state.imageSource} ;
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'trainer', reducer });

const TrainerContainer = compose(withReducer, withConnect)(TrainerComponent);
export default TrainerContainer;


const initialState = ({
    score: {},
    imageSource: ""
    // gameItems: [],
    // gameItemsFound: [],
    // finalScore: '',
    // isCameraRunning: false,
    // currentItem:{},
    // nextItem: {},
    // // remainingTime,
    // isGameFinished:false
});

function trainerReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_IMAGE_SOURCE":
        // 
            return { ...state, imageSource: action.payload.imageSource }
        default:
            return state;
    }
    return;
}
export default trainerReducer;

import { KNNImageClassifier } from 'deeplearn-knn-image-classifier';
import * as dl from 'deeplearn';
import FileSaver from 'file-saver';
import { axiosPost } from '../utils/axios-api'

// Number of classes to classify
const NUM_CLASSES = 10;
// Webcam Image size. Must be 227.
const IMAGE_SIZE = 227;
// K value for KNN
const TOPK = 10;
snapShotCanvas: HTMLCanvasElement;
// Initiate deeplearn.js math and knn classifier objects
let knn;

export function createKnnImageClassifier(numberOfItems, numberOfImagesPerItem){
    knn = new KNNImageClassifier(numberOfItems, numberOfImagesPerItem);
    knn.load().then(() => {console.log("knn loaded");});
    // return knn;
}
// export function addImage(knn, image, imageClass) {
//     
//     knn.addImage(image, imageClass)
// }
export function addImage( image, imageClass) {
    // let snapShotCanvas = document.createElement('canvas');
    for(let i=0; i<10; i++ ){
        knn.addImage(image,imageClass )
    }    
}
// export function checkImageForAMatch(knn,image,expectedImageClass){
//     
//     var classIndex = predictImage(knn,image);
// }
export function checkImageForAMatch(image,expectedImageClassIndex){
    predictImage(image).then((res) =>{
        if(res.classIndex == expectedImageClassIndex) return true;
        else return false
    });
}
export function takeImage(video){
    const image = dl.fromPixels(video);
    return image;
}
export function predictImage(image){
    return knn.predictClass(image)
        // .then((res)=>{
        //     console.log(res);
        //     return res.classIndex
        // })
}

export function uploadTrainedItemToServer(itemName) {
    // 
    let trainingData = "";
    if(knn.getClassLogitsMatrices.length > 1000){
        const logits = knn.getClassLogitsMatrices();
        const tensors = logits.map((t) => {
        if (t) {
            return t.dataSync();
        }
        return null;
        });
        trainingData = JSON.stringify({ logits, tensors }) ;
    }
    else{
        trainingData = ""
    }
    
    let data = {
        "data" : {
            "itemName": itemName, 
            "itemLevel": 1, 
            "inputTrainingData" : trainingData,
            "isActive" : true,
            "description" : "Description about the item"
        }
    }
    axiosPost("game",data)
  }

  export function uploadImagesToServer(itemName, imageStrings){
    // 
    // const trainingData = JSON.stringify({ logits, tensors }) ;
    let data = {
        "data" : {
            "itemName": itemName, 
            "images": imageStrings, 
            "isActive" : true,
        }
    }
    axiosPost("images",data)
  }
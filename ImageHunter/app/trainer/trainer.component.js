/*
 * Home Page
 *
 */
import React, { PureComponent } from 'react';
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet';
import './trainer.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Webcam from 'react-webcam';
import { createKnnImageClassifier, addImage, checkImageForAMatch, takeImage, uploadTrainedItemToServer, uploadImagesToServer } from './trainer.service'
import FileInput from 'react-simple-file-input';

class TrainerComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.uploadImages = this.uploadImages.bind(this);
        this.uploadImagesToServerFunction = this.uploadImagesToServerFunction.bind(this);
        this.uploadTrainedItemToServerFunction = this.uploadTrainedItemToServerFunction.bind(this);
    }
    currentItemToTrain = "Add Custom Item Name"
    itemImages = [];

    getCanvasFromImage(imageFile) {
        return new Promise((resolve, reject) => {
            let canvas = document.getElementById('canvas');
            let ctx = canvas.getContext("2d");
            var FR = new FileReader();
            FR.onload = (e) => {
                console.log("FIle Opened");
                resolve(e.target.result.replace(/(\r\n\t|\n|\r\t)/gm, ""));
            }
            FR.readAsDataURL(imageFile);

        });
    }

    componentDidMount() {
        createKnnImageClassifier(1, 50);
    }
    async uploadImages(imageFiles) {
        try {
            for (let i = 0; i < imageFiles.length; i++) {
                console.log("did I wait ?");
                let canvasToTrain = await this.getCanvasFromImage(imageFiles[i]);
                console.log(canvasToTrain);
                this.itemImages.push(canvasToTrain);
                // let image = takeImage(canvasToTrain);
                // addImage(image, 0);
            }
        }
        catch (Error) {
            // 
            console.log(Error);
        }
    }

    uploadTrainedItemToServerFunction = () => {
        uploadTrainedItemToServer(this.currentItemToTrain);
    }
    updateItemNameValue = (e) => {
        this.currentItemToTrain = e.target.value
    }
    uploadImagesToServerFunction = (e) => {
        uploadImagesToServer(this.currentItemToTrain, this.itemImages);
    }
    render() {

        return (
            <section>
                <Helmet>
                    <title>Trainer</title>
                    <meta name="description" content="Train Game Items" />
                </Helmet>
                <div className="landing-page">
                    <div className="limiter">
                        <input className="form-control" placeholder="Item Name" onChange={this.updateItemNameValue} />
                        <FileInput
                            readAs='binary'
                            multiple={true}
                            // onLoadStart={this.showProgressBar}
                            // onLoad={this.handleFileSelected}
                            // onProgress={this.updateProgressBar}
                            // cancelIf={this.checkIfFileIsIncorrectFiletype}
                            // abortIf={this.cancelButtonClicked}
                            // onCancel={this.showInvalidFileTypeMessage}
                            // onAbort={this.resetCancelButtonClicked}
                            onChange={(images) => { this.uploadImages(images) }}
                        />
                        <img id='test' style={{ display: 'block', height: '100' }} src={this.props.imageSource} alt={"Image Not Loaded"} />

                        <canvas id="canvas" ></canvas>
                        <button >Train new object</button>
                        <button> Clear Training Data </button>
                        <button onClick={this.uploadTrainedItemToServerFunction}> Upload this item to server </button>
                        <button onClick={this.uploadImagesToServerFunction}>Upload Images </button>

                    </div>
                </div>
            </section>
        );
    }
}

TrainerComponent.propTypes = {

};


export default TrainerComponent;

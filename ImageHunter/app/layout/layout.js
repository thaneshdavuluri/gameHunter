import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';

import NotFoundPage from '../not-found' ;
import HomePage from '../home/home.container';
import GamePage from '../game/game.container';
import TrainerPage from '../trainer/trainer.container';


import './style.scss';
import ScrollToTop from './scroll-to-top';

const App = () => (
  <div >
    <Helmet
      titleTemplate="%s - IRN"
      defaultTitle="IRN"
    >
      <meta name="description" content="Image Recognition Novartis" />
    </Helmet>
    {/* <Header /> */}
    {/* <SpinnerContainer /> */}
    {/* <NotificationsContainer /> */}
    {/* <ReduxToastr
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      preventDuplicates
    /> */}
    <div className="container-fluid content-top-margin body-content">
      <ScrollToTop>
        <Switch>
          {/* <Route exact path="/" component={HomePage} /> */}
          <Route exact path="/" component={GamePage} />
          <Route exact path="/trainer" component={TrainerPage} />
          {/* <Route exact path="/dashboard" component={DashboardPage} /> */}
          {/* <Route exact path="/" component={LoginPage} /> */}
          <Route path="" component={NotFoundPage} />
        </Switch>
      </ScrollToTop>

    </div>
  </div>
);

export default App;

import axios from 'axios';


function getWithBaseApiUrl(url) {
  return `${process.env.API_SERVER_URL}/${url}`;
}

export function axiosPost(url, data) {
  return (
    axios({
      url: getWithBaseApiUrl(url),
      method: 'POST',
      data: JSON.stringify(data.data),
      headers: data.headers || {
        'Content-Type': 'application/json'
      },
      responseType: 'json'
    })
  );
}

export function axiosLocalGet(url, queryString = null) {
  let getUrl = url;
  if (queryString) {
    getUrl = `${getUrl}?${queryString}`;
  }
  return (
    axios.get((getUrl))
      .catch((error) => {
        if (error.response) {
          // The request was made, but the server responded with a status code
          // that falls out of the range of 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);
      })
  );
}
export function axiosGet(url, queryString = null) {
  let getUrl = url;
  if (queryString) {
    getUrl = `${getUrl}?${queryString}`;
  }
  return (
    axios.get(getWithBaseApiUrl(getUrl))
      .catch((error) => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        }else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
        console.log(error.config);
      })
  );
}

export function XMLHttpRequestSyncGet(url, queryString = null) {
  let getUrl = url;
  if (queryString) {
    getUrl = `${getUrl}?${queryString}`;
  }
  try {
    const request = new XMLHttpRequest();
    request.open('GET', getWithBaseApiUrl(getUrl), false); // `false` makes the request synchronous
    request.send(null);

    if (request.status === 200) {
      return JSON.parse(request.response);
    }
    console.error(getUrl, 'Status: ', request.status);
    return [];
  } catch (error) {
    console.error(getUrl, 'Error: ', error);
  }
}


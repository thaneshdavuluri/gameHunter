import * as _ from 'lodash';
import { ResponseStatusEnum } from '../../constants/enums/response-status-enum';

const SHOW_NOTIFICATIONS = 'SHOW_NOTIFICATIONS';
const CLEAR_NOTIFICATIONS = 'CLEAR_NOTIFICATIONS';


/**
 * notify to user
 * @param {string[] | string} messages
 * @param {ResponseStatusEnum} errorType
 * @param {boolean} persistMessage
 */
export function notify(messages, errorType, persistMessage) {
  return (dispatch) => {
    if (!messages) {
      return;
    }
    let msgArr = [];
    if (messages.constructor === Array) {
      msgArr = msgArr.concat(messages);
    } else {
      msgArr.push(messages);
    }
    const messagesConfig = {};
    messagesConfig.messages = _.map(msgArr, (msg) => ({
      displayMessage: msg.Message || msg.displayMessage || msg,
      normalizedMessage: msg.normalizedMessage || msg.NormalizedMessage, params: msg.params,
    }));
    messagesConfig.type = errorType || ResponseStatusEnum.Success;
    messagesConfig.persistMessages = persistMessage || false;

    dispatch({
      type: SHOW_NOTIFICATIONS,
      messagesConfiguration: messagesConfig,
    });
  };
}

/**
 *
 * @param {string} message
 * @param {text:string,handler:function} buttons
 */
export function custom(message, buttons) {
  return (dispatch, getState) => {
    const messagesConfig = {};
    messagesConfig.message = { displayMessage: message.Message || message, normalizedMessage: message.NormalizedMessage };
    messagesConfig.type = ResponseStatusEnum.Custom;
    messagesConfig.persistMessages = false;

    _.each(buttons, (btn, i) => {
      if (i === 0) { btn.ok = true; }
    });
    messagesConfig.buttons = buttons || [];

    dispatch({
      type: SHOW_NOTIFICATIONS,
      messagesConfiguration: messagesConfig,
    });
  };
}

/**
 *
 * @param {string} message
 * @param {text:string,handler:function} buttons
 */
export function confirm(message, buttons) {
  return (dispatch, getState) => {
    const messagesConfig = {};
    messagesConfig.message = { displayMessage: message.Message || message, normalizedMessage: message.NormalizedMessage, params: message.params };
    messagesConfig.type = ResponseStatusEnum.Confirmation;
    messagesConfig.persistMessages = false;
    _.each(buttons, (btn, i) => {
      if (i == 0) {
        btn.ok = true;
        btn.onOk = btn.handler;
      }
      if (i == 1) {
        btn.cancel = true;
        btn.onCancel = btn.handler;
      }
    });
    messagesConfig.buttons = buttons || [];

    dispatch({
      type: SHOW_NOTIFICATIONS,
      messagesConfiguration: messagesConfig,
    });
  };
}

export function clearNotifications() {
  return {
    type: CLEAR_NOTIFICATIONS
  };
}

const initialState = {
  type: ResponseStatusEnum.Error,
  persistMessages: false,
  buttons: [],
  messages: [],
  message: '',
  id: -1,
  notify,
  clearNotifications,
  custom,
  confirm
};

function notificationReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_NOTIFICATIONS: {
      return Object.assign({}, state, {
        id: Date.now() + Math.floor(Math.random() * 10000),
        messages: action.messagesConfiguration.messages || [],
        persistMessages: action.messagesConfiguration.persistMessages,
        type: action.messagesConfiguration.type,
        func: action.messagesConfiguration.func,
        buttons: action.messagesConfiguration.buttons || [],
        message: action.messagesConfiguration.message || ''
      });
    }
    case CLEAR_NOTIFICATIONS: {
      return Object.assign({}, state, {
        type: ResponseStatusEnum.Error,
        messages: [],
        persistMessages: false,
        func: null,
      });
    }
    default:
      return state;
  }
}

export default notificationReducer;


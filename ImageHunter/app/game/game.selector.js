/**
 * Home selectors
 */

import { createSelector } from 'reselect';

const IsTrainingCompletedFlagSelector = (state) => (state.game ? state.game.trainingComplete : false);

const getIsTrainingCompletedFlag = () => createSelector(
    IsTrainingCompletedFlagSelector,
    (isTrainingComplete) => { return isTrainingComplete }
);
const initialCountdownTimerSelector =  (state) => (state.game ? state.game.initialCountdownTimer : 0)
// const remainingTime = (state) => (state.game ? state.game.remainingTime : 0);

// const getRemainingTime = () => createSelector(
//     remainingTime,
//     (remainingTime) => { return remainingTime }
// );
const getInitialCountdownTimer = () => createSelector(
    initialCountdownTimerSelector ,
    (time) => {  return time}
)
const getIsRoundCompletedSelector = (state) => (state.game ? state.game.isGameRoundCompleted : false);

const getIsRoundCompleted = () => createSelector(
    getIsRoundCompletedSelector,
    (isGameRoundCompleted) => { return isGameRoundCompleted }
);  

export {
    getIsTrainingCompletedFlag,
    getInitialCountdownTimer,
    getIsRoundCompleted
};

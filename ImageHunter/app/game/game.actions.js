import {uploadFinishedGameScore} from './game.service';

export function setGameItems(gameItems) {
    return (dispatch, getState) => {
        let remainingGameItems = gameItems;
        if (getState().game.gamePlayCount >= 1) {
            remainingGameItems = getState().game.gameItemsNotFound;
        } else {
            _.map(gameItems, (gameItem) => {
                gameItem.notFoundCount = 0;
            })
            console.log(gameItems);
        }

        const shuffledGameItems = _.shuffle(remainingGameItems);
        const payload = {
            gameItems: gameItems,
            remainingGameItems: shuffledGameItems
        };
        dispatch({
            type: 'SET_GAME_ITEMS',
            payload
        });
    };
}

export function setInitialCountdownTimer(time) {
    return (dispatch, getState) => {
        let e = getState().game.initialCountdownTimer;
        dispatch({
            type: 'SET_INITIAL_COUNTDOWN_TIMER',
            payload: { "time": 10000 - (Math.random() * 10) }
        });
    }
}



export function getGameItems() {
    return (dispatch, getState) => {
        return getState().game.gameItems
    }
}
export function calculateScoreForItem(initialScoreForItem, timeTaken) {
    let remainingTime = (60 - timeTaken);
    let score = 0;
    if (remainingTime < 10) score = 5;
    else if (remainingTime > 10 && remainingTime < 20) score = 10;
    else if (remainingTime > 20 && remainingTime < 30) score = 15;
    else if (remainingTime > 30 && remainingTime < 40) score = 20;
    else if (remainingTime > 40 && remainingTime < 50) score = 25;
    else if (remainingTime > 50) score = (35 - timeTaken);

    return initialScoreForItem + score;
}
export function gameItemDiscovered(gameItem, timeTaken) {
    return (dispatch, getState) => {
        let foundItem = {
            "gameItem": gameItem,
            "timeTaken": timeTaken
        }
        let totalTimeTaken = getState().game.totalTimeTaken + timeTaken;
        let alreadyFoundItems = getState().game.gameItemsFound;
        let remainingGameItems = getState().game.remainingGameItems;
        let initialScore = getState().game.finalScore;
        alreadyFoundItems.push(foundItem);
        _.remove(remainingGameItems, (item) => {
            return item.item == gameItem.item
        })
        const finalScore = calculateScoreForItem(initialScore, timeTaken)
        const payload = {
            "alreadyFoundItems": alreadyFoundItems,
            "remainingGameItems": remainingGameItems,
            "finalScore": finalScore,
            "totalTimeTaken" : totalTimeTaken
        }
        dispatch({
            type: "GAME_ITEM_DISCOVERED",
            payload
        });
    }
}
export function setIsGameRoundCompleted(isGameRoundCompleted) {
    return (dispatch, getState) => {
        dispatch({
            type: "SET_IS_GAME_ROUND_COMPLETED",
            payload: { "isGameRoundCompleted": isGameRoundCompleted }
        })
    }
}
export function gameItemNotFound() {
    return (dispatch, getState) => {
        const remainingGameItems = getState().game.remainingGameItems;
        const notFoundItems = getState().game.gameItemsNotFound;
        const currentGameItem = remainingGameItems[0];

        if(currentGameItem.hasOwnProperty("notFoundCount")) currentGameItem.notFoundCount += 1;
        else {
            currentGameItem.notFoundCount = 1 ;
        }

        _.remove(remainingGameItems, (item) => {
            return item.item == currentGameItem.item
        })
        _.remove(notFoundItems , (item) => {return item.item == currentGameItem.item} )
        notFoundItems.push(currentGameItem);
        let timeTaken = getState().game.totalTimeTaken + 60 ;
        const payload = {
            notFoundItems: notFoundItems,
            remainingGameItems: remainingGameItems,
            "totalTimeTaken" : timeTaken
        }

        dispatch({
            type: "GAME_ITEM_NOT_FOUND",
            payload
        });
    }
}

export function getGameItemsNotFound() {
    return (dispatch, getState) => {
        return getState().game.gameItemsNotFound
    }
}

export function getGamePlayCount() {
    return (dispatch, getState) => {
        return getState().game.gamePlayCount
    }
}
export function getRemainingTime() {
    return (dispatch, getState) => {
        return getState().game.remainingTime;
    }
}
export function getImageLastCaptured() {
    return (dispatch, getState) => {
        return getState().game.imageLastCaptured;
    }
}
export function getFinalScore() {
    return (disptach, getState) => {
        return getState().game.finalScore
    }
}
export function increaseGamePlayCount() {
    return (dispatch, getState) => {
        let gamePlayCount = getState().game.gamePlayCount;
        const payload = {
            "gamePlayCount": gamePlayCount + 1
        }
        dispatch({
            type: "GAME_PLAY_COUNT",
            payload
        });
    }

}
export function saveUserDetails(user){
    return (dispatch) =>{
        const payload = {
            "user":user
        }
        dispatch({
            type: "SAVE_USER_DETAILS",
            payload
        })
    }
}

export function finishGame() {
    return (dispatch, getState) => {
        // make api call
        let name =  getState().game.user.fullName
        let id = getState().game.user.officeID
        let attempts = getState().game.gamePlayCount
        let timetaken = getState().game.totalTimeTaken
        let date = new Date()
        let data = {
            "EmployeeName":name,
            "EmployeeId": id ,
            "Attempts": attempts,
            "TimeTaken":timetaken,
            "GamePlayDate": date,
            "IsActive": true
        }
        uploadFinishedGameScore(data);
    }
}

export function isTrainingComplete(trainingComplete) {
    return (dispatch, getState) => {
        const payload = {
            "trainingComplete": trainingComplete
        }
        console.log("training complete: ", trainingComplete);
        dispatch({
            type: "TRAINING_COMPLETE",
            payload
        });
    }
}
export function isGameFinished() {
    return (dispatch, getState) => {
        if (getState().game.remainingGameItems.length == 0) {
            return true;
        }
        else return false;
    }
}
export function getNextGameItem() {
    console.log("Next item loaded");
    // prodchange
    return (dispatch, getState) => {
        if (getState().game.remainingGameItems.length >= 0) {
            let nextGameItem = getState().game.remainingGameItems[0];
            dispatch(setTimer(60));
            return nextGameItem;
        }
        else {
            dispatch(setTimer(0));
            return null;
        }
    }
}

export function reduceTimerBySeconds(time) {
    return (dispatch, getState) => {
        let remainingTime = getState().game.remainingTime;
        remainingTime -= time;
        const payload = {
            remainingTime: remainingTime
        }
        dispatch({
            type: 'REDUCE_REMAINING_TIME',
            payload
        });
    };
}

export function setTimer(time) {
    return (dispatch, getState) => {
        const payload = {
            "remainingTime": time
        }
        dispatch({
            type: 'SET_REMAINING_TIME',
            payload
        });
    };
}
export function setImageLastCaptured(timeOfCapture) {
    return (dispatch, getState) => {
        const payload = {
            imageLastCaptured: timeOfCapture
        }
        dispatch({
            type: 'SET_IMAGE_LAST_CAPTURED',
            payload
        });
    };
}

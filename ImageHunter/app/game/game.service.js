import { KNNImageClassifier } from 'deeplearn-knn-image-classifier';
import * as dl from 'deeplearn';
import FileSaver from 'file-saver';
import * as _ from 'lodash';
import { axiosGet,axiosPost } from '../utils/axios-api'
import axios from 'axios';

// Number of classes to classify
const NUM_CLASSES = 10;
// Webcam Image size. Must be 227.
const IMAGE_SIZE = 227;
// K value for KNN
const TOPK = 10;
snapShotCanvas: HTMLCanvasElement;
// Initiate deeplearn.js math and knn classifier objects
let knn;

export function createKnnImageClassifier(numberOfItems, numberOfImagesPerItem) {
    knn = new KNNImageClassifier(numberOfItems, numberOfImagesPerItem);
    return knn.load();
    // return knn;
}

// export function checkImageForAMatch(image, expectedImageClassIndex) {
//     predictImage(image).then((res) => {
//         if (res.classIndex == expectedImageClassIndex) return true; // add check for confidence score
//         else return false
//     });
// }
export function takeImage(video) {
    // 
    // const image = dl.fromPixels(x);
    const image = dl.fromPixels(video);
    return image;
}
export function predictImage(image) {
    // 
    return knn.predictClass(image)
}
export async function loadPreTrainedImagesToKNNFromServer(trainedDataWithImagesFromServer) {
    let gameItems = []; let i = 0;
    try {
        for (let i = 0; i <= trainedDataWithImagesFromServer.length; i++) {
            gameItems.push({
                "item": trainedDataWithImagesFromServer[i].itemName,
                "level": i
            })
            for (let j = 0; trainedDataWithImagesFromServer[i].images && j <= trainedDataWithImagesFromServer[i].images.length; j++) {
                let imageData = await convertURIToImageData(trainedDataWithImagesFromServer[i].images[j]);
                if(imageData){
                    let imageInTensor = dl.fromPixels(imageData);
                    for(let x = 0; x <= 20 ; x++){
                        knn.addImage(imageInTensor, i);

                    }
                }
            }
            // _.map(trainedDataWithImagesFromServer[0].images, (image) => {

            // })
            // i++;
        }
    } catch (error) {
        //console.log(error);
    }

    // _.map((trainedDataWithImagesFromServer), (gameItem) => {
    //     gameItems.push({
    //         "item": gameItem.itemName,
    //         "level": i
    //     })

    //     _.map(gameItem.images, (image) => {

    //         let imageInTensor = dl.fromPixels(image);
    //         knn.addImage(imageInTensor, i);
    //     })
    //     i++;
    // })
    console.log("Yes");
    return gameItems
}

function convertURIToImageData(URI) {
    return new Promise(function (resolve, reject) {
        if (URI == null)  resolve(undefined);
        var canvas = document.createElement('canvas'),
            context = canvas.getContext('2d'),
            image = new Image(227, 227);
        image.addEventListener('load', function () {
            canvas.width = image.width;
            canvas.height = image.height;
            context.drawImage(image, 0, 0, canvas.width, canvas.height);
            resolve(context.getImageData(0, 0, canvas.width, canvas.height));
        }, false);
        image.src = URI;
    });
}
export function loadPreTrainedDataToKNNFromServer(trainedDataFromServer) {
    // 
    let tensors = []
    let gameItems = []; let level = 0;
    _.map(trainedDataFromServer, (itemData) => {
        gameItems.push({
            "item": itemData.itemName,
            "level": level
        })
        const data = JSON.parse(itemData.inputTrainingData);
        let tensor = data.tensors.map((tensor, i) => {
            if (tensor) {
                const values = Object.keys(tensor).map(v => tensor[v]);
                return dl.tensor(values, data.logits[i].shape, data.logits[i].dtype);
            }
            return null;
        });
        tensor.map((tenso) => {
            tensors.push(tenso);
        })
        level++;
    });
    knn.setClassLogitsMatrices(tensors);
    return gameItems
}

export function getGameItemsWithImagesFromServer() {
    return axios.get('http://13.58.75.218:8000/api/images');
    // return axios.get('localhost:8000/api/images');
}

export function getGameItemsFromServer() {
    return axios.get('http://13.58.75.218:8000/api/game');
    // return axios.get('localhost:8000/api/game');
}

export function uploadFinishedGameScore(data){
    return axiosPost('scores',data)
    // return axios.post('localhost:8000/api/scores',data)
}
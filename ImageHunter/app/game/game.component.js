/*
 * Home Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './game.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Webcam from 'react-webcam';
import { checkImageForAMatch, takeImage, loadPreTrainedDataToKNNFromServer, loadPreTrainedImagesToKNNFromServer, getGameItemsFromServer, getGameItemsWithImagesFromServer, predictImage, createKnnImageClassifier } from './game.service'
import Countdown from 'react-countdown-now';
import Timer from "../assets/images/icons/alarm-clock.png"
// import Timer from '../countdown-timer/cd-timer.component'

class GameComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.stopGameFunction = this.stopGameFunction.bind(this);
        this.startGame = this.startGame.bind(this);
        this.restartGame = this.restartGame.bind(this);
        this.getGameScreen = this.getGameScreen.bind(this);
        this.getLoadingNextitemScreen = this.getLoadingNextitemScreen.bind(this);
        this.getLoadingGameScreen = this.getLoadingGameScreen.bind(this);
        this.getAllItemsFoundScreen = this.getAllItemsFoundScreen.bind(this);
        this.getLoginPage = this.getLoginPage.bind(this);
        this.saveUserDetails = this.saveUserDetails.bind(this);
        // this.renderGame = this.renderGame.bind(this);
        this.finishGame = this.finishGame.bind(this);
        this.state = {
            countdown: 60, newGameCountDown: 0, isLoggedIn: false
            , fullName: '', officeID: '', email: ''
        };
    }
    // remainingTime = 60;
    stopGame = false;
    currentItemFound = false;
    currentItem = {};
    gameItems = [];

    countdown = 0;
    timer;
    videoConstraints = {
        width: 1280,
        height: 720,
        facingMode: 'environment',
    };
    setTimer(time) {
        this.setState({ countdown: time });
        if (this.timer) {
            clearInterval(this.timer);
        }
        this.timer = setInterval(this.tick, 1000);
    }

    tick = () => {
        // This function is called every 50 ms. It updates the
        // elapsed counter. Calling setState causes the component to be re-rendered
        this.setState({ countdown: this.state.countdown - 1 });
        if (this.state.countdown <= 0) {
            this.setState({ countdown: 0 });
            clearInterval(this.timer);
        }
    }
    updateName = (e) => {
        this.setState({ fullName: e.target.value })
    }
    updateEmail = (e) => {
        this.setState({ email: e.target.value })
    }
    updateOfficeID = (e) => {
        this.setState({ officeID: e.target.value })
    }
    saveUserDetails = (e) => {
        e.preventDefault();
        const user = {
            "fullName": this.state.fullName,
            "email": this.state.email,
            "officeID": this.state.officeID
        }
        this.props.saveUserDetails(user);
        this.setState({ isLoggedIn: true });
    }
    // getInitialState(){
    //
    //     // This is called before our render function. The object that is
    //     // returned is assigned to this.state, so we can use it later.
    //
    //     return { countdown: 0 };
    // }

    componentDidMount() {
        let trainedDataFromServer = [];
        // let gameItems = []
        // getGameItemsFromServer().then((res) => {
        this.props.isTrainingComplete(false);
        getGameItemsWithImagesFromServer().then((res) => {
            trainedDataFromServer = res.data;
            let numberOfItemClasses = trainedDataFromServer.length;
            createKnnImageClassifier(numberOfItemClasses, 10).then(async () => {
                this.gameItems = (await loadPreTrainedImagesToKNNFromServer(trainedDataFromServer));
                console.log("training Complete")
                this.props.setGameItems(this.gameItems);
                this.props.isTrainingComplete(true);
                // this.startGame();
            });
        })
    }
    getAllItemsFoundScreen = () => {
        return (
            <div className="wrap-login100 Countdown">
                <span className="login100-form-title p-b-26" style={{ marginTop: '50%', fontSize: "26px !important " }}>
                </span>
                <div className="scorecard">
                    <span>Your Score : </span>
                    <span>{this.props.getFinalScore()}</span>
                </div>
                <button className="login100-form-btn"> Finish Game </button>

                <div className="message">
                    <span>You have successfully completed the activity.  Look out for exciting prizes</span>
                </div>
            </div>

        )
    }
    startGame() {
        this.props.increaseGamePlayCount();
        this.loadNextItemFromStore();
        this.stopGame = false;
        // startTimer()
        this.keepGameRunning();
    }
    itemFound = () => {
        console.log("found before timer went down");
        this.currentItemFound = true;
        let timeTaken = 60 - this.state.countdown;
        this.props.foundGameItem(this.currentItem, timeTaken);
    }

    itemNotFound = () => {
        console.log("not found after timer went down");
        this.props.gameItemNotFound();
    }
    loadNextItemFromStore = async () => {
        let currentItem = this.props.getNextGameItem();
        console.log(currentItem);
        if (currentItem) {
            this.currentItem = currentItem;
            // this.props.setInitialCountdownTimer(10000);
            // await this.nextItemWait();
            this.setTimer(60);
            this.currentItemFound = false;
        }
    }
    getFewItemsFoundScreen = () => {
        return (
            <div className="wrap-login100 Countdown">
                <span className="login100-form-title p-b-26" style={{ marginTop: '50%', fontSize: "26px !important " }}>
                    Missed A Few Items
                </span>

                <button onClick={this.restartGame} className="login100-form-btn"> Restart Game </button>
                <button onClick={this.finishGame} className="login100-form-btn"> Finish Game </button>
                <div className="message">
                    <span>Click on Restart to search again for undiscovered items.</span>
                </div>
            </div>
        )
    }
    nextItemWait() {
        return new Promise((resolve, reject) => {
            this.setState({ newGameCountDown: 3 });
            let nextItemTimer = setInterval(() => {
                if (this.state.newGameCountDown > 0) {
                    this.setState({ newGameCountDown: this.state.newGameCountDown - 1 });
                    if (this.state.newGameCountDown == 0) {
                        clearInterval(nextItemTimer);
                        return resolve();
                    }
                }
            }, 1000);
        });
    }

    async keepGameRunning() {
        if(this.props.isGameRoundCompleted) return ;
        if (this.state.countdown > 0) {

            try {
                var res = await this.searchForCurrentItem();
                if (res.classIndex == this.currentItem.level && res.confidences[this.currentItem.level] >= 0.75) {
                    // if (true) {
                    // console.log("item found");
                    this.itemFound();
                    await this.nextItemWait();
                    this.loadNextItemFromStore();
                }
            } catch (ex) {
                console.log(ex)
            }
        } else {
            this.itemNotFound();
            await this.nextItemWait();
            this.loadNextItemFromStore();
        }

        if (this.props.isGameFinished()) {
            this.props.setIsGameRoundCompleted(true);
            return;
        }

        return setTimeout(() => {
            this.keepGameRunning();
        }, 100);
    }

    async restartGame() {
        console.log("game restarting");
        let gamePlayCount = this.props.getGamePlayCount();
        let gameItemsNotFound = this.props.getGameItemsNotFound();
        let newGameItems = gameItemsNotFound.filter(item => item.notFoundCount == ( gamePlayCount ) );
        this.props.setGameItems(newGameItems);
        console.log("new game items:", newGameItems);
        this.props.setIsGameRoundCompleted(false);
        // this.startGame();
    }

    async finishGame() {
        this.props.finishGame();
    }

    searchForCurrentItem() {
        debugger;
        let currentItem = this.currentItem;
        if (currentItem) {
            // let video = document.createElement("VIDEO");
            // let video = _.cloneDeep(this.webcam.video)
            // let video = {...this.webcam.video};
            let video = this.webcam.video;
            video.height = 227;
            video.width = 227;
            let image = takeImage(video);
            return predictImage(image);
        }
    }
    getGameScreen = () => {
        const videoConstraints = {
            width: 1280,
            height: 720,
            facingMode: 'environment',
        };
        return (<div>
            <div className="game-camera-screen">
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100 Countdown">
                            <div className="finder">
                                {/* <span style={{ fontSize: '16px' }}>Find Chair</span> */}
                                <span style={{ fontSize: '16px' }}>Find {this.currentItem.item}</span>
                            </div>
                            <div className="timerclock">
                                <span>
                                    <img src={Timer} alt="timer" height="24" width="24" />
                                </span>
                                <span id="Imagecountdowntimer">{this.state.countdown}</span>
                            </div>
                            <br />
                            <div style={{ marginTop: '-5%' }}><span style={{ color: 'white', fontSize: "12px" }}>Description</span></div>
                            {/* <div style={{ marginTop: '-5%' }}><span style={{ color: 'white', fontSize: "12px" }}>{this.currentItem.description}</span></div> */}
                            <div className="camerascanner game-camera-screen">
                                <Webcam ref={this.setRef} audio={false} width={'100%    '} screenshotFormat="image/jpeg" screenshotQuality={1} videoConstraints={this.videoConstraints} onUserMedia={this.startGame} />
                            </div>
                            <button className="login100-form-btn" onClick={this.stopGameFunction}>
                                STOP GAME
                    </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
    getLoadingNextitemScreen = () => {
        return (
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100 Countdown">
                        <span className="login100-form-title p-b-26" style={{ marginTop: "50%" }}>
                            #GetINSPIREady
                            {this.state.newGameCountDown}
                        </span>
                        <span className="loading-message p-b-26"> Loading Next Item. </span>
                        <div className="meter">
                            <span style={{ width: "25%" }}></span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    getLoadingGameScreen = () => {
        return (
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100 Countdown">
                        <span className="login100-form-title p-b-26" style={{ marginTop: "50%" }}>
                            #GetINSPIREady
                        </span>
                        <span className="loading-message p-b-26"> Loading Game. Please wait </span>
                        <div className="meter">
                            <span style={{ width: "25%" }}></span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    getLoginPage = () => {
        return (
            <div className="landing-page">
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100">
                            <form className="login100-form validate-form">
                                <span className="login100-form-title p-b-26">
                                    #GetINSPIREady
                                </span>
                                <div>
                                    <h3 className="scan">SCAN & WIN</h3>
                                    <p className="matter">Locate the elements asked for, with your phone camera. Earn points on finding the right elements and win exciting prizes. Make sure your phone is not in silent/mute mode.</p>
                                </div>
                                <div className="login-card">
                                    <div className="wrap-input100 validate-input">
                                        <input className="input100" type="text" name="Fullname" onChange={this.updateName} value={this.state.fullName} />
                                        <span className="focus-input100" data-placeholder="Full Name"></span>
                                    </div>
                                    <div className="wrap-input100 validate-input">
                                        <input className="input100" type="text" name="officeID" onChange={this.updateOfficeID} value={this.state.officeID} />
                                        <span className="focus-input100" data-placeholder="Office ID"></span>
                                    </div>
                                    <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                                        <input className="input100" type="text" name="email" onChange={this.updateEmail} value={this.state.email} />
                                        <span className="focus-input100" data-placeholder="Email"></span>
                                    </div>
                                </div>
                                <button
                                    type='button'
                                    onClick={this.saveUserDetails}
                                    className="login100-form-btn">
                                    LET'S PLAY
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    stopGameFunction = () => {
        this.stopGame = true
    }

    setRef = (webcam) => {
        this.webcam = webcam;
    }

    render() {

        const isTrainingCompleted = this.props.isTrainingCompletedFlag;
        //
        let gameComp = "";

        return (
            <section>
                <Helmet>
                    <title>Game</title>
                    <meta name="description" content="Game" />
                </Helmet>
                {!this.state.isLoggedIn ?
                    this.getLoginPage()
                    :
                    (this.props.isGameRoundCompleted ?
                        (
                            <div className="limiter">
                                <div className="container-login100">
                                    {
                                        this.props.getGameItemsNotFound().length > 0 ? this.getFewItemsFoundScreen() : this.getAllItemsFoundScreen()}
                                </div>
                            </div>
                        )
                        :
                        (<div className="landing-page">
                            <div className="limiter">
                                {
                                    this.props.isTrainingCompletedFlag ?
                                        (
                                            // this.state.newGameCountDown > 0 ?
                                            //     this.getLoadingNextitemScreen() : this.getGameScreen()
                                            this.getGameScreen()

                                        )
                                        : (this.getLoadingGameScreen())
                                }
                            </div>
                        </div>)
                    )
                }
            </section>
            // this.getGameScreen()
        );
    }
}



GameComponent.propTypes = {

};


export default GameComponent;

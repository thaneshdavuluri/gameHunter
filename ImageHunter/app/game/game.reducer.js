
const initialState = ({
    score: {},
    gameItems: [],
    gameItemsFound: [],
    gameItemsNotFound: [],
    remainingGameItems: [],
    finalScore: 0,
    isCameraRunning: false,
    // remainingTime,
    isGameFinished: false,
    trainingComplete: false,
    gamePlayCount: 0,
    imageLastCaptured: new Date(),
    remainingTime: 60,
    initialCountdownTimer: 60,
    isGameRoundCompleted: false,
    totalTimeTaken: 0,
    user:{}
});

function gameReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_GAME_ITEMS":
            return { ...state, gameItems: action.payload.gameItems, remainingGameItems: action.payload.remainingGameItems };
        case "GAME_ITEM_DISCOVERED":
            return { ...state, remainingGameItems: action.payload.remainingGameItems.map((x) => ({ ...x })), gameItemsFound: action.payload.alreadyFoundItems.map((x) => ({ ...x })), finalScore: action.payload.finalScore, totalTimeTaken: action.payload.totalTimeTaken };


        case "GAME_ITEM_NOT_FOUND":
            return {
                ...state, remainingGameItems: action.payload.remainingGameItems.map((x) => ({ ...x })),
                gameItemsNotFound: action.payload.notFoundItems.map((x) => ({ ...x })),
                totalTimeTaken: action.payload.totalTimeTaken
            }
        case "SAVE_USER_DETAILS":
            return { ...state, user: action.payload.user }
        case "TRAINING_COMPLETE":
            return {
                ...state, trainingComplete: action.payload.trainingComplete
            }
        case 'REDUCE_REMAINING_TIME':
            return {
                ...state, remainingTime: action.payload.remainingTime
            };
        case 'SET_REMAINING_TIME':
            return {
                ...state, remainingTime: action.payload.remainingTime
            }
        case 'SET_IMAGE_LAST_CAPTURED':
            return {
                ...state, imageLastCaptured: action.payload.imageLastCaptured
            };
        case 'GAME_PLAY_COUNT':
            return {
                ...state, gamePlayCount: action.payload.gamePlayCount
            }
        case 'SET_INITIAL_COUNTDOWN_TIMER':
            return {
                ...state, initialCountdownTimer: action.payload.time
            }
        case 'SET_IS_GAME_ROUND_COMPLETED':
            return {
                ...state, isGameRoundCompleted: action.payload.isGameRoundCompleted
            }
        default:
            return state;
    }
}

export default gameReducer;



export function saveUserDetails(user){
    return (dispatch) =>{
        const payload = {
            "user":user
        }
        dispatch({
            type: "SAVE_USER_DETAILS",
            payload
        })
    }
}
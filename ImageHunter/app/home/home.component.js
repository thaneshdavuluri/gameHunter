/*
 * Home Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './home.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'
import * as _ from 'lodash';

class HomeComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.getButton = this.getButton.bind(this);
        this.state = {
            fullName: '',
            officeID: '',
            email: ''
        }
    }
    getButton = () => {
        let startButton = withRouter(({ history }) => (
            <button
                type='button'
                onClick={() => {
                    this.saveUserDetails
                    history.push('/game')
                }} className = "login100-form-btn"
            >
                LET'S PLAY
            </button>
        ))
        return startButton;
    }
    
    componentDidMount() {
    }
    updateName = (e) => {
        this.setState({ fullName: e.target.value })
    }
    updateEmail = (e) => {
        this.setState({ email: e.target.value })
    }
    updateOfficeID = (e) => {
        this.setState({ officeID: e.target.value })
    }
    saveUserDetails = (e) => {
        e.preventDefault();
        const user = {
            "fullName": this.state.fullName,
            "email": this.state.email,
            "officeID": this.state.officeID
        }
        this.props.saveUserDetails(user);
        this.history.pushState(null, 'game');
    }
    render() {
        return (
            <section>
                <Helmet>
                    <title>Home</title>
                    <meta name="description" content="Home" />
                </Helmet>
                <div className="landing-page">
                    <div className="limiter">
                        <div className="container-login100">
                            <div className="wrap-login100">
                                <form className="login100-form validate-form">
                                    <span className="login100-form-title p-b-26">
                                        #GetINSPIREady
                                    </span>
                                    <div>
                                        <h3 className="scan">SCAN & WIN</h3>
                                        <p className="matter">Locate the elements asked for, with your phone camera. Earn points on finding the right elements and win exciting prizes. Make sure your phone is not in silent/mute mode.</p>
                                    </div>
                                    <div className="login-card">
                                        <div className="wrap-input100 validate-input">
                                            <input className="input100" type="text" name="Fullname" onChange={this.updateName} value={this.state.fullName} />
                                            <span className="focus-input100" data-placeholder="Full Name"></span>
                                        </div>
                                        <div className="wrap-input100 validate-input">
                                            <input className="input100" type="text" name="officeID" onChange={this.updateOfficeID} value={this.state.officeID} />
                                            <span className="focus-input100" data-placeholder="Office ID"></span>
                                        </div>
                                        <div className="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                                            <input className="input100" type="text" name="email" onChange={this.updateEmail} value={this.state.email} />
                                            <span className="focus-input100" data-placeholder="Email"></span>
                                        </div>
                                    </div>
                                    {this.getButton()}
                                    {/* <button className="login100-form-btn" onClick={}>
                                        LET'S PLAY
                                    </button> */}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

HomeComponent.propTypes = {

};

export default HomeComponent;

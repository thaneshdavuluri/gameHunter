﻿using System;

namespace models
{
    public class GameItemModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int itemLevel { get; set; }
        public string inputTrainingData { get; set; }
        public bool isActive { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace models
{
    public class ImageDataModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public List<string> Images { get; set; }
    }
}